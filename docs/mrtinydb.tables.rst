``mrtinydbutils.tables`` -- Tables for TinyDB
=============================================

.. automodule:: mrtinydbutils.tables

``TableUuid``
-------------

.. autoclass:: mrtinydbutils.tables.TableUuid
   :members:
   :private-members:

``TableTimestamp``
------------------

.. autoclass:: mrtinydbutils.tables.TableTimestamp
   :members:
   :private-members:
   :special-members:

``TableUuidTimestamp``
----------------------

.. autoclass:: mrtinydbutils.tables.TableUuidTimestamp
   :members:
   :private-members:
   :special-members:
