``mrtinydbutils.middlewares`` -- Middlewares for TinyDB
==========================================================

.. automodule:: mrtinydbutils.middlewares

``DocIdToEntryMiddleware``
--------------------------

.. autoclass:: mrtinydbutils.middlewares.DocIdToEntryMiddleware
   :members:
