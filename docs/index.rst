=======================================
Welcome to mrtinydbutils documentation!
=======================================

User guide
==========
.. toctree::
   :maxdepth: 3

   userguide

Interface description
=====================
.. toctree::
   :maxdepth: 2

   mrtinydb

General information
===================

.. toctree::
   :maxdepth: 1

   todo

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
