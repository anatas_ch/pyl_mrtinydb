``mrtinydbutils`` -- TinyDB Utilities
========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   mrtinydb.tables
   mrtinydb.middlewares



``mrtinydbutils`` -- TinyDB Utilities
-------------------------------------

.. automodule:: mrtinydbutils
   :members:

``mrtinydbutils.version`` -- Version information
------------------------------------------------

.. py:module:: mrtinydbutils.version

.. py:data:: version
   :type: str

   Current Version.

.. py:data:: version_tuple
   :type: Tuble[int, int, int, Optional[str], Optional[str]]

   Current Version.
