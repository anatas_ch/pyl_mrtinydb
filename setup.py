# -*- coding: utf-8 -*-

"""Setuptools setup file, used to install or test `mrtinydbutils`."""

from setuptools import setup


setup(
    use_scm_version=True,
)
