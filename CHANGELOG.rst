=========
Changelog
=========

All notable changes to this project will be documented in this file.

The format is based on `Keep a Changelog <https://keepachangelog.com/en/1.0.0/>`__,
and this project adheres to `Semantic Versioning <https://semver.org/spec/v2.0.0.html>`__.


[Unreleased]
============

[0.1.1] - 2021-06-06
====================

Added
-----
- This CHANGELOG file.

Changed
-------
- Documentation: Link to Read the Doc

Fixed
-----
- CI: run all test files. Results in a lower coverage.


[0.1.0] - 2021-06-05
====================

Added
-----
- first public release.
